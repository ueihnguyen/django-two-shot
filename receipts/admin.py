from django.contrib import admin

# Register your models here.

from receipts.models import Account, Receipt, ExpenseCategory


class ReceiptAdmin(admin.ModelAdmin):
    pass


class AccountAdmin(admin.ModelAdmin):
    pass


class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Receipt, ReceiptAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)
